#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <sstream>
#include <algorithm>

void framebufferResizeCallback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods);
void glfwErrorCallback(int error, const char* description);
void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam);

const char* vsSource = "#version 450 core		\n"
"layout (location = 0) in vec3 pos;				\n"
"layout (location = 1) in vec2 texcoord;		\n"

"out vec2 uv;									\n"

"void main()									\n"
"{												\n"
"	uv = texcoord;								\n"
"	gl_Position = vec4(pos, 1.0f);				\n"
"}												\n\0";

const char* fsSource = "#version 450 core\n"
"in vec2 uv;									\n"

"out vec4 color;								\n"

"uniform sampler2D mipTexture;					\n"
"uniform float uSelectedMipLevel;				\n"

"void main()									\n"
"{												\n"
"	color = textureLod(mipTexture, uv, uSelectedMipLevel);			\n"
//"		color = vec4(uv, 0, 1);				\n"
"}												\n\0";

const char* fsColorSource = "#version 450 core\n"
"in vec2 uv;									\n"

"out vec4 color;								\n"

"uniform vec3 uColor;					\n"

"void main()									\n"
"{												\n"
"		color = vec4(uColor, 1);				\n"
"}												\n\0";

int textureWidth = 800;
int textureHeight = 600;

int wWidth = 800;
int wHeight = 600;

uint32_t selectedLodLevel = 0;

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(wWidth, wHeight, "Mipmap Clear Example", nullptr, nullptr);

	if (window == nullptr)
	{
		std::cout << "Failed to create window" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);
	glfwSetErrorCallback(glfwErrorCallback);

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize glew" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(glewErrorCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GLFW_TRUE);

	int success;
	char infoLog[512];
	
	int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vsSource, nullptr);
	glCompileShader(vertexShader);

	int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fsSource, nullptr);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, nullptr, infoLog);
		std::cout << "Error compiling fragment shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int fragColorShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragColorShader, 1, &fsColorSource, nullptr);
	glCompileShader(fragColorShader);

	int program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(program, 512, nullptr, infoLog);
		std::cout << "Error compiling program\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	int mipColorProgram = glCreateProgram();
	glAttachShader(mipColorProgram, vertexShader);
	glAttachShader(mipColorProgram, fragColorShader);
	glLinkProgram(mipColorProgram);

	glGetProgramiv(mipColorProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(mipColorProgram, 512, nullptr, infoLog);
		std::cout << "Error compiling program\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
	glDeleteShader(fragColorShader);

	int mipTextureLocation = glGetUniformLocation(program, "mipTexture");
	int mipSelectionLocation = glGetUniformLocation(program, "uSelectedMipLevel");
	int mipColorUniformLocation = glGetUniformLocation(mipColorProgram, "uColor");

	float hquad[] =
	{
		// position(vec3), uv(vec2)
		0.5f, 0.5f, 0.0f,	1.0f, 1.0f,
		0.5f,-0.5f, 0.0f,	1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f, 0.0f, 0.0f,
		-0.5f, 0.5f, 0.0f,	0.0f, 1.0f
	};

	float fxquad[] =
	{
		// position(vec3), uv(vec2)
		1.0f, 1.0f, 0.0f,	1.0f, 1.0f,
		1.0f,-1.0f, 0.0f,	1.0f, 0.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,	0.0f, 1.0f
	};

	unsigned int indices[] =
	{
		0, 1, 3,
		1, 2, 3
	};

	unsigned int final_vao, vbo_hquad, ib;
	glGenVertexArrays(1, &final_vao);
	glGenBuffers(1, &vbo_hquad);
	glGenBuffers(1, &ib);

	glBindVertexArray(final_vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_hquad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(hquad), hquad, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	glBindVertexArray(0);

	unsigned int mip_vao, vbo_fxquad;
	glGenVertexArrays(1, &mip_vao);
	glGenBuffers(1, &vbo_fxquad);

	glBindVertexArray(mip_vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_fxquad);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fxquad), fxquad, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);

	glBindVertexArray(0);

	unsigned int mipTextureId;
	glGenTextures(1, &mipTextureId);
	glBindTexture(GL_TEXTURE_2D, mipTextureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, textureWidth, textureHeight, 0, GL_RGBA, GL_FLOAT, nullptr);

	uint32_t maxLodLevels = 1 + floor(std::log2(std::max(textureWidth, textureHeight)));
	std::cout << "Generating Lod for [" << textureWidth << ", " << textureHeight << "] Levels: " << maxLodLevels << std::endl;
	glGenerateMipmap(GL_TEXTURE_2D);

	unsigned int mipFbo;
	glCreateFramebuffers(1, &mipFbo);
	glDepthMask(GL_FALSE);
	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		// write color
		glBindFramebuffer(GL_FRAMEBUFFER, mipFbo);
		glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mipTextureId, 0);
		glViewport(0, 0, textureWidth, textureHeight);
		glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
		glClear(GL_COLOR_BUFFER_BIT);

		glBindVertexArray(mip_vao);
		glUseProgram(mipColorProgram);
		// set uniform color
		glUniform3f(mipColorUniformLocation, 1.f, 0.5f, 0.5f);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		// final 
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, wWidth, wHeight);
		glClearColor(0.2f, 0.2f, 0.2f, .2f);
		glClear(GL_COLOR_BUFFER_BIT);

		glBindVertexArray(final_vao);
		glUseProgram(program);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mipTextureId);
		glUniform1i(mipTextureLocation, 0);

		glUniform1f(mipSelectionLocation, selectedLodLevel);

		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteTextures(1, &mipTextureId);
	glDeleteFramebuffers(1, &mipFbo);
	glDeleteProgram(program);

	glDeleteVertexArrays(1, &final_vao);
	glDeleteVertexArrays(1, &mip_vao);

	glDeleteBuffers(1, &vbo_hquad);
	glDeleteBuffers(1, &vbo_fxquad);
	glDeleteBuffers(1, &ib);

	glfwTerminate();
	return 0;
}

void framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
	wWidth = width;
	wHeight = height;
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods)
{
	if(key >= GLFW_KEY_KP_0 && key <= GLFW_KEY_KP_9 && action == GLFW_RELEASE)
	{
		selectedLodLevel = key - GLFW_KEY_KP_0;
		std::cout << "Selected current lod to: " << selectedLodLevel << std::endl;
	}

	if(key >= GLFW_KEY_0 && key <= GLFW_KEY_9 && action == GLFW_RELEASE)
	{
		selectedLodLevel = key - GLFW_KEY_0;
		std::cout << "Selected current lod to: " << selectedLodLevel << std::endl;
	}
}

void glfwErrorCallback(int error, const char* description)
{
	std::cout << "GLFW error: " << error << " \n: " << description;
}

void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
		return;

	const auto startsWith = [](const char *str, const char* pre)
	{
		const auto lenpre = strlen(pre);
		const auto lenstr = strlen(str);
		return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
	};

	// skip shader recompile performance warning on Intel chips
	// TODO: what does this message mean ... there is no information about this warning available online
	if (source == GL_DEBUG_SOURCE_API && type == GL_DEBUG_TYPE_PERFORMANCE && severity == GL_DEBUG_SEVERITY_MEDIUM && startsWith(message, "API_ID_RECOMPILE_FRAGMENT_SHADER"))
		return;

	std::stringstream msg;

	msg << "GL Error Callback: ID=" << id << ": " << message;

	int errorId = id;
	int errorSource = 0;
	int errorType = 0;
	int errorSeverity = 0;

	msg << ", ";
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             msg << "Source: API"; errorSource = 1; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   msg << "Source: Window System"; errorSource = 2; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: msg << "Source: Shader Compiler"; errorSource = 3; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     msg << "Source: Third Party"; errorSource = 4; break;
	case GL_DEBUG_SOURCE_APPLICATION:     msg << "Source: Application"; errorSource = 5; break;
	case GL_DEBUG_SOURCE_OTHER:           msg << "Source: Other"; errorSource = 6; break;
	}

	msg << ", ";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:				msg << "Type: Error"; errorType = 1; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: msg << "Type: Deprecated Behaviour"; errorType = 2; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  msg << "Type: Undefined Behaviour"; errorType = 3; break;
	case GL_DEBUG_TYPE_PORTABILITY:         msg << "Type: Portability"; errorType = 4; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         msg << "Type: Performance"; errorType = 5; break;
	case GL_DEBUG_TYPE_MARKER:              msg << "Type: Marker"; errorType = 6; return;			// ignore marker, push group & pop group messages
	case GL_DEBUG_TYPE_PUSH_GROUP:          msg << "Type: Push Group"; errorType = 7; return;		// are used to mark specific stages of the rendering (eg. renderdoc)
	case GL_DEBUG_TYPE_POP_GROUP:           msg << "Type: Pop Group"; errorType = 8; return;		// but they aren't actually error messages
	case GL_DEBUG_TYPE_OTHER:				msg << "Type: Other"; errorType = 9; break;
	}

	msg << ", ";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         msg << "Severity: high"; errorSeverity = 4; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       msg << "Severity: medium"; errorSeverity = 3; break;
	case GL_DEBUG_SEVERITY_LOW:          msg << "Severity: low"; errorSeverity = 2; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: msg << "Severity: notification"; errorSeverity = 1; break;
	}

	// todo find out how "severe" this really is
	bool logNotifications = true;
	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION && !logNotifications)
		return;

	std::cout << msg.str() << std::endl;
}